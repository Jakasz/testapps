package jakasz.testappforwork

import android.app.AlertDialog
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.setPadding
import com.android.volley.Response
import com.android.volley.toolbox.ImageRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.NetworkImageView
import com.android.volley.toolbox.Volley
import jakasz.testappforwork.dataClasses.usersLists
import jakasz.testappforwork.utils.CustomToast
import jakasz.testappforwork.utils.ImageRequester
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject


class MainScreen : AppCompatActivity() {
    private var userToken = ""
    var userId: Int = 0
    var userNickname: String = ""
    var ViewsCount: Int = 0
    var BoockMarksCount: Int = 0
    var RepostCount: Int = 0
    var CommentCount: Int = 0
    var LikesCount  : Int = 0
    var usersWhoLikeedPost = ArrayList<usersLists>()
    var usersWhoRepostedpost = ArrayList<usersLists>()
    var usersWhoCommentpost = ArrayList<usersLists>()
    var userWhoLinkedToPost = ArrayList<usersLists>()
    var usersWhoMentionPost = ArrayList<usersLists>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED
        userToken = getString(R.string.rsToken)
        val getLikesByMethod = GetLikes()
        val getPostReposters = GetPostData(usersWhoRepostedpost, getString(R.string.reMethRepost))
        val getPostComment = GetPostData(usersWhoCommentpost, getString(R.string.rsMethCommend))
        val getPostMentions = GetPostData(userWhoLinkedToPost, getString(R.string.rsMethLinked))
        val tvVC = findViewById<TextView>(R.id.tvViewCount)
        tvCommentsMore.setOnClickListener {
            showCustomToast(tvCommentsMore)
        }
        tvLikesMore.setOnClickListener {
            showCustomToast(tvLikesMore)
        }
        tvMentionsMore.setOnClickListener {
            showCustomToast(tvMentionsMore)
        }
        tvRepostersMore.setOnClickListener{
            showCustomToast(tvRepostersMore)
        }
        /**Getting user id by user slug id**/
        val loadingProgressbar = AlertDialog.Builder(this)
        loadingProgressbar.setCancelable(false)
        loadingProgressbar.setView(R.layout.progress_bar_wait)
        val loadDialog = loadingProgressbar.create()
        loadDialog.show()
        getMainListData(
            Handler(Handler.Callback {
                when (it.arg1) {
                    1 -> {
                        //Лучше с ресурс стрингс
                        tvVC.text= "Просмотры: $ViewsCount"
                            //Вызываем методы для получения данных после
                            //получения основной информации о посте
                            //Можно сделать универсальную процедуру для всех методов, пока остановился на данном варианте
                            //Лайки
                            getLikes(getLikesByMethod)
                            //Репосты
                            getReposters(getPostReposters)
                            //Коммменты
                            getComments(getPostComment)
                            //Отметки
                            getMentions(getPostMentions)

                    }
                    else -> {
                        CustomToast().showToast(this, "Nothing to show, sorry")
                    }

                }
                loadDialog.dismiss()
                false
            })
        )
    }
    //Сообщение о ивенте клик(т.к. нету другого активити для остальных пользователей)
    private fun showCustomToast(textView : TextView) {
        if (textView.visibility!=View.GONE) {
            CustomToast().showToast(
                this,
                "Здесь были бы остальные люди данной категории"
                , Gravity.TOP, Toast.LENGTH_LONG
            )
        }
    }

    private fun getMentions(postMentions: GetPostData) {
            postMentions.getPostData(Handler(Handler.Callback {
                when (it.arg1) {
                    1 -> {
                        usersWhoMentionPost = postMentions.userDataArray
                        createImages(usersWhoMentionPost, R.id.horizontalImagesMentions, tvMentionsMore, usersWhoMentionPost.size)
                        //Скрываем кнопку еще если количество постов меньше отображаемых
                        if (usersWhoMentionPost.size<=10){
                            tvMentionsMore.visibility=View.GONE
                        }
                        //Лучше с ресурс стрингс
                        tvMentions.text="Отметки: ${usersWhoMentionPost.size}"
                    }
                    else -> {
                    }
                }
                false
            }), this, userToken, userId)

    }

    private fun getComments(postComment: GetPostData) {
        if (CommentCount>0){
        postComment.getPostData(Handler(Handler.Callback {
            when (it.arg1) {
                1 -> {
                    usersWhoCommentpost = postComment.userDataArray
                    createImages(usersWhoCommentpost, R.id.horizontalImagesComments, tvCommentsMore, CommentCount)
                    //Скрываем кнопку еще если количество постов меньше отображаемых
                    if (usersWhoCommentpost.size<=10){
                        tvCommentsMore.visibility=View.GONE
                    }
                    //Лучше с ресурс стрингс
                    tvCommentsCount.text= "Комментарии: $CommentCount"
                }
                else -> {
                }
            }
            false
        }), this, userToken, userId)}
        else
        {
            tvCommentsMore.visibility = View.GONE
        }
    }

    private fun getReposters(getRepostersData: GetPostData) {
        if (RepostCount>0){
        getRepostersData.getPostData(Handler(Handler.Callback {
            when (it.arg1) {
                1 -> {
                    usersWhoRepostedpost = getRepostersData.userDataArray
                    createImages(usersWhoRepostedpost, R.id.horizontalImagesReposts, tvRepostersMore, RepostCount)
                    //Скрываем кнопку еще если количество постов меньше отображаемых
                    if (usersWhoRepostedpost.size<=10){
                        tvRepostersMore.visibility=View.GONE
                    }
                    //Лучше с ресурс стрингс
                    tvRepostCount.text= "Репосты: $RepostCount"
                }
                else -> {
                }
            }
            false
        }), this, userToken, userId)}
        else
        {
            tvRepostersMore.visibility=View.GONE
        }
    }

    private fun getLikes(getLikesByMethod: GetLikes) {
        if (LikesCount>0) {
            getLikesByMethod.getPostLikes(
                Handler(Handler.Callback {
                    when (it.arg1) {
                        1 -> {
                            usersWhoLikeedPost = getLikesByMethod.usersWhoLikedPost
                            createImages(usersWhoLikeedPost, R.id.horizontalImages, tvLikesMore, usersWhoLikeedPost.size)
                                //Скрываем кнопку еще если количество постов меньше отображаемых
                                    if (usersWhoLikeedPost.size<=10){
                                        tvLikesMore.visibility=View.GONE
                                    }
                            //Лучше с ресурс стрингс
                            tvLikesCount.text = "Лайки: ${usersWhoLikeedPost.size}"
                        }
                        else -> {
                            usersWhoLikeedPost.clear()
                        }
                    }
                    false
                }), this, userToken, userId
            )
        }
        else{
            tvLikesMore.visibility=View.GONE
        }
    }

    private fun createImages(usersData: ArrayList<usersLists>, layout: Int, tvMoreCaption : TextView, vCount: Int) {
        val iterationCount : Int
        val horizontalView = findViewById<LinearLayout>(layout)
        if (usersData.size > 10) {
            //Лучше с ресурс стрингс
            tvMoreCaption.text= "Еще:"+ (vCount-10)
               iterationCount = 10
        } else {
            iterationCount = usersData.size
        }
        for (i in 0 until iterationCount) {
            //Лейаут для 2х компонентов
            val linearLay = LinearLayout(this)
            linearLay.orientation = LinearLayout.VERTICAL
            //вью для изображения
            val userImageView = NetworkImageView(this)
            //вью для имени пользователя
            val userNameForImage = TextView(this)
            linearLay.addView(userImageView)
            linearLay.addView(userNameForImage)
            //параметры для для корректного отображения имени
            val userNameLayParams = LinearLayout.LayoutParams(
                200,
                80
            )
            userNameForImage.gravity = TextView.TEXT_ALIGNMENT_CENTER
            userNameForImage.layoutParams = userNameLayParams
            userNameForImage.textSize = 12F
            userNameForImage.gravity = Gravity.CENTER_HORIZONTAL
            userImageView.setPadding(10)
            horizontalView.addView(linearLay)
            userNameForImage.text = usersData[i].Name
            //получаем изображение

            ImageRequester(this).setImageFromUrl(userImageView, usersData[i].urlImage)


//            val requestQueue = Volley.newRequestQueue(applicationContext)
//            val iQue = ImageRequest(usersData[i].urlImage,
//                Response.Listener<Bitmap?> { response -> userImageView.setImageBitmap(response) },
//                200, 200, Bitmap.Config.RGB_565,
//                Response.ErrorListener { Log.e("1", "Image Load Error: ") })
//
//            requestQueue.add(iQue)
        }
    }

        private fun getMainListData(handler: Handler) {
        val msg = Message.obtain()
        msg.target = handler
        msg.what = 1
        val url = "https://api.inrating.top/v1/users/posts/get"
        val startQuery = Volley.newRequestQueue(this)
                //id зашил в метод. в задании не было указаний как его вводить
        val postJson = JSONObject("{\"slug\":\"4JZfTIgB45vh\"}")
        val finishQuery = object : JsonObjectRequest(
            Method.POST,
            url,
            postJson,
            Response.Listener { response ->
                val jsonStringObject = JSONObject(response.toString())
                Thread(
                    Runnable {
                        try {
                            val id = jsonStringObject.optInt("id")
                            val nickname = jsonStringObject.optString("nickname")
                            userId = id
                            userNickname = nickname
                            ViewsCount = jsonStringObject.optInt("views_count")
                            BoockMarksCount = jsonStringObject.optInt("bookmarks_count")
                            CommentCount = jsonStringObject.optInt("comments_count")
                            RepostCount = jsonStringObject.optInt("reposts_count")
                            LikesCount = jsonStringObject.optInt("likes_count")

                        } catch (e: Exception) {
                            Log.d("Exception : ", e.message.toString())
                        } finally {
                            msg.arg1 = 1
                            msg.sendToTarget()

                        }
                    }).start()
            },
            Response.ErrorListener {
                msg.arg1 = -1
                msg.sendToTarget()
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
//            headers["Content-Type"] = "application/json"
//            headers["Authorization"] = "Basic $encodedString"
                headers["Authorization"] = "Bearer $userToken"
                return headers
            }
        }

        startQuery.add(finishQuery)

    }


}
