package jakasz.testappforwork.utils

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import jakasz.testappforwork.R

class CustomToast {
    // Custom Toast Method
    @SuppressLint("InflateParams")
    fun showToast(
        context: Context,
        error: String,
        gravity: Int? = Gravity.TOP,
        duration: Int? = Toast.LENGTH_LONG
    ) {
        val layout = LayoutInflater.from(context).inflate(R.layout.custom_toast, null)
        val text = layout.findViewById(R.id.toast_error) as TextView
        text.text = error
        val toast = Toast(context)
        toast.setGravity(gravity!!, 0, 0)
        toast.duration = duration!!// Set Duration
        toast.view = layout
        toast.show()
    }
}