package jakasz.testappforwork

import android.content.Context
import android.os.Handler
import android.os.Message
import android.util.Log
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import jakasz.testappforwork.dataClasses.usersLists
import org.json.JSONObject

class GetLikes{
    var usersWhoLikedPost = ArrayList<usersLists>()

    fun getPostLikes(handler: Handler, context: Context, userToken : String, id : Int) {
        val msg = Message.obtain()
        msg.target = handler
        msg.what = 1
        val url = "https://api.inrating.top/v1/users/posts/likers/all"
        val startQuery = Volley.newRequestQueue(context)
        val postJson = JSONObject("{\"id\":\"$id\"}")
        val finishQuery = object : JsonObjectRequest(
            Method.POST,
            url,
            postJson,
            Response.Listener { response ->
                val jsonStringObject = JSONObject(response.toString())
                Thread(
                    Runnable {
                        try {
                              val usersArray = jsonStringObject.getJSONArray("data")
                                for (i in 0 until usersArray.length()-1){
                                    val userArrayObj = usersArray.getJSONObject(i)
                                    val avatars = userArrayObj.getJSONObject("avatar_image")
                                    usersWhoLikedPost.add(usersLists(userArrayObj.getInt("id"),
                                        userArrayObj.getString("name"),
                                        avatars.getString("url_medium")))
                                }

                        } catch (e: Exception) {
                            Log.d("Exception : ", e.message.toString())
                        } finally {
                            msg.arg1 = 1
                            msg.sendToTarget()

                        }
                    }).start()

            },
            Response.ErrorListener {
                val message = it.networkResponse
                msg.arg1 = -1
                msg.sendToTarget()
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
//            headers["Content-Type"] = "application/json"
//            headers["Authorization"] = "Basic $encodedString"
                headers["Authorization"] = "Bearer $userToken"
                return headers
            }
        }

        startQuery.add(finishQuery)
    }
}