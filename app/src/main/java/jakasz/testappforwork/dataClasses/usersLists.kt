package jakasz.testappforwork.dataClasses

import android.os.Parcel
import android.os.Parcelable

class usersLists(private val id : Int, val Name : String, val urlImage : String) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!

    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(Name)
        parcel.writeString(urlImage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<usersLists> {
        override fun createFromParcel(parcel: Parcel): usersLists {
            return usersLists(parcel)
        }

        override fun newArray(size: Int): Array<usersLists?> {
            return arrayOfNulls(size)
        }
    }

}
